﻿using System;

namespace Hackathon.Data
{
    public class Fail
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string  Description { get; set; }
        public int Vote { get; set; }
        public float Rate { get; set; }
        public Guid? ImageId { get; set; }
    }
}