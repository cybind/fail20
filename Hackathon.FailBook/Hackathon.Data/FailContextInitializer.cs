﻿using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace Hackathon.Data
{


    public class FailContextInitializerContextInitializer :DropCreateDatabaseIfModelChanges<FailContext>
    {
        protected override void Seed(FailContext context)
        {
            new List<Fail>
                {
                    new Fail
                        {
                            Id = Guid.NewGuid(),
                            Title = "Упал с крыши",
                            Description = "Устроил девушке романтический вечер, но что-то пошло не так...",
                            Rate = 1
                        },
                    new Fail
                    {
                        Id = Guid.NewGuid(),
                        Title = "Нога застряла в туалете... общественном",
                        Description = "Ненавижу сортиры",
                        Rate = 2
                    }
                }.ForEach(s => context.Fails.Add(s));
            base.Seed(context);
        }

    }
}
