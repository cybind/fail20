﻿using System.Data.Entity;


namespace Hackathon.Data
{
    public class FailContext : DbContext
    {
        public DbSet<Fail> Fails { get; set; }


        public FailContext()
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<FailContext>());
            Database.SetInitializer(new FailContextInitializerContextInitializer());
        }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<Bug>().HasOptional(a => a.Category);

        //    modelBuilder.Entity<Bug>()
        //        .HasOptional(a => a.AssignedTo)
        //        .WithMany()
        //        .HasForeignKey(u => u.AssignedToId);

        //    modelBuilder.Entity<Bug>()
        //        .HasRequired(a => a.CreatedBy)
        //        .WithMany()
        //        .HasForeignKey(u => u.CreatedById);
        //}

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<SlideShowData>().HasMany(s=>s.SlideShowAudios).WithOptional().WillCascadeOnDelete(true);
        //    modelBuilder.Entity<SlideShowData>().HasMany(s => s.SlideShowImages).WithOptional().WillCascadeOnDelete(true);
        //    modelBuilder.Entity<SlideShowData>().HasMany(s => s.SlideShowVideos).WithOptional().WillCascadeOnDelete(true);

        //    //modelBuilder.Entity<SlideShowData>().HasOptional((s => s.SlideShowImages)).WithOptionalPrincipal().
        //    //   WillCascadeOnDelete();

        //    //modelBuilder.Entity<SlideShowData>().HasOptional((s => s.SlideShowVideos)).WithOptionalPrincipal().
        //    //   WillCascadeOnDelete();
        //}
    }
}
