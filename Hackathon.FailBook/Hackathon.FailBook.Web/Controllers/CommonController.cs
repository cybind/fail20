﻿using System;
using System.IO;
using System.Web.Mvc;
using Hackathon.Data;
using Hackathon.FailBook.Web.Models;
using Hackathon.FailBook.Web.Properties;
using System.Collections.Generic;
using System.Linq;

namespace Hackathon.FailBook.Web.Controllers
{
    public class CommonController : BaseController
    {
        private readonly FailRepository _failRepository = new FailRepository();

        [HttpPost]
        public ActionResult SaveFail(FailAddModel failModel)
        {
            var fail = new Fail();
            
            fail.Id = Guid.NewGuid();
            fail.Title = failModel.Title;
            fail.Description = failModel.Description;

            if (failModel.ImageId != Guid.Empty)
                fail.ImageId = failModel.ImageId;
            else
                fail.ImageId = null;

            var response = new ResponseBase { IsError = _failRepository.Create(fail) == null};

            return Json(response);
        }

        public ActionResult AddImage()
        {
            var files = Request.Files;

            if (files.Count > 0)
            {
                var file = files[0];
                if (file != null)
                {
                    var imgId = Guid.NewGuid();
                    file.SaveAs(Path.Combine(Settings.Default.UploadPath, string.Format("{0}.png",imgId,file.FileName)));
                    return Json(imgId, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(false, JsonRequestBehavior.AllowGet);
            
        }

        public ActionResult GetImage(string id)
        {
            return File(Path.Combine(Settings.Default.UploadPath,string.Format("{0}.{1}",id,"png")),"image/png");
        }

        public ActionResult Vote(string id)
        {
            var resp = new ModelsResponse<Fail>();

            Guid guid;
            if (Guid.TryParse(id, out guid))
            {
                var fail = _failRepository.Find(f => f.Id == guid);
                fail.Vote++;
                //CalculateRate(fail);
                resp.IsError = _failRepository.Update(fail) <= 0;

                //resp.Models = GetTopFails();
            } else
                resp.IsError = true;

            return Json(resp, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetTopFailListPartial()
        {
            return PartialView("TopFailsListPartial", GetTopFails());
        }

        public List<Fail> GetTopFails()
        {
            return _failRepository.All().OrderByDescending(f => f.Vote).Take(10).ToList();
        }

        //public void CalculateRate(Fail fail)
        //{
        //    //int voteThreshold = 500;

        //    var allFails = _failRepository.All().OrderByDescending(f => f.Vote).ToList();
        //    var index = allFails.FindIndex(f => f.Id == fail.Id) + 1;

        //    //float averageRate = allFails.Sum(f => f.Rate) / 2.0f;
        //    //float averageVote = allFails.Sum(f => f.Vote) / 2.0f;

        //    //float rate = ((fail.Vote / (float)(fail.Vote + voteThreshold)) * fail.Vote) + ((voteThreshold / (float)(fail.Vote + voteThreshold)) * averageRate);
        //    fail.Rate = index;

        //}
    }
}