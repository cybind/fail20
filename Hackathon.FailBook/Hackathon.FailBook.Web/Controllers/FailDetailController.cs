﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Hackathon.Data;
using Hackathon.FailBook.Web.Models;

namespace Hackathon.FailBook.Web.Controllers
{
    public class FailDetailController : CommonController
    {
        private readonly FailRepository _repository = new FailRepository();

        public ActionResult Index(string id = null)
        {
            var model = new FailDetailModel();

            var failId = id != null ? new Guid(id) : Guid.NewGuid();
            var result = _repository.Find(f => f.Id == failId) ??
                         new Fail { Title = "Ем после 18:00", Description = "Однажды съел слона после 18.00 и чуть не сдох...", Rate = 17, Vote = 167, ImageId = new Guid("8afd2239-e326-4609-bb8f-ec7b998a05a0") };

            result.Rate = _repository.All().OrderByDescending(f => f.Vote).ToList().FindIndex(f => f.Id == result.Id) + 1;

            model.TopFails = this.GetTopFails();
            model.FailInfo = result;
            //model.ImageUrl = this.GetImage(result.ImageId.ToString());

            

            return View("FailDetail", model);
        }
    }
}
