﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Hackathon.Data;
using Hackathon.FailBook.Web.Models;

//using Hackathon.Data;

namespace Hackathon.FailBook.Web.Controllers
{
    public class HomeController : CommonController
    {
        private readonly FailRepository _repository = new FailRepository();

        public ActionResult Index()
        {
            var topFails = GetTopFails();

            return View(topFails);
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult GetForCompare()
        {
            var resp = new ModelsResponse<Fail>();

            var fails = _repository.All().ToList();

            if (fails.Count > 1)
            {
                var rnd = new Random((int)DateTime.Now.Ticks);
                var first = rnd.Next(0, fails.Count - 1);
                int second = -1;
                while (second == first || second < 0)
                    second = rnd.Next(0, fails.Count);

                resp.Models.Add(fails[first]);
                resp.Models.Add(fails[second]);
            } 
            else
                resp.IsError = true;

            return Json(resp, JsonRequestBehavior.AllowGet);
        }
    }
}
