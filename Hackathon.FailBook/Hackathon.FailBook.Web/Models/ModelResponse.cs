﻿using System.Collections.Generic;

namespace Hackathon.FailBook.Web.Models
{
    public class ModelsResponse<T> : ResponseBase
    {
        public ModelsResponse()
        {
            Models = new List<T>();
        }

        public List<T> Models { get; set; }
    }
}