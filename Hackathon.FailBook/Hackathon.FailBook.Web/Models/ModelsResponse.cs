﻿namespace Hackathon.FailBook.Web.Models
{
    public class ModelResponse<T> : ResponseBase
    {
        public T Model { get; set; }
    }
}