﻿namespace Hackathon.FailBook.Web.Models
{
    public class ResponseBase
    {
        public bool IsError { get; set; }
    }
}