﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Hackathon.Data;

namespace Hackathon.FailBook.Web.Models
{
    public class FailDetailModel
    {
        public Fail FailInfo { get; set; }
        public List<Fail> TopFails { get; set; }

        public string ImageUrl { get; set; }

        public FailDetailModel()
        {
            TopFails = new List<Fail>();
        }
    }
}