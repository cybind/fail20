﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hackathon.FailBook.Web.Models
{
    public class FailAddModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Guid ImageId { get; set; }
    }
}