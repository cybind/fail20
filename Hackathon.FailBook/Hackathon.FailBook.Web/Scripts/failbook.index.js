﻿var firstElem;
var secondElem;
$(document).ready(function () {
    firstElem = $('#firstFail');
    secondElem = $('#secondFail');
    GetFailForCompare();
});

function GetFailForCompare() {
    var offset = 600;

    firstElem.animate({ left: "-=" + offset + "px" }, "fast", function () { firstElem.css("opacity", "1"); });
    secondElem.animate({ left: "+=" + offset + "px" }, "fast", function () { secondElem.css("opacity", "1"); });

    $.ajax({
        type: "GET",
        url: "/Home/GetForCompare",
        dataType: "json",
        contentType: 'application/json',
        success: function (result) {
            if (!result.IsError) {
                firstElem.unbind('click').click(function () { Vote(result.Models[0].Id, firstElem); });
                firstElem.find('#id').text(result.Models[0].Id);
                firstElem.find('#title').text(result.Models[0].Title).attr('href', '/FailDetail/Index/' + result.Models[0].Id + "?rand=" + Math.random());
                firstElem.find('#title').attr('title', result.Models[0].Title);
                firstElem.find('#description').text(result.Models[0].Description);
                firstElem.find('#description').attr('title', result.Models[0].Description);
                firstElem.find('.vote').fadeOut(0).text(result.Models[0].Vote);
                if (result.Models[0].ImageId != null) {
                    firstElem.find('#picture').attr('src', '/Common/GetImage/' + result.Models[0].ImageId);
                    firstElem.find('#picture').parent().parent().show();
                    // if there is picture than show truncated description
                    firstElem.find(".compared #description").css('overflow', 'hidden');
                    firstElem.find(".compared #description").css('white-space', 'nowrap');
                }
                else {
                    firstElem.find('#picture').parent().parent().hide();
                    // if there is no picture than show full description
                    firstElem.find(".compared #description").css('overflow', '');
                    firstElem.find(".compared #description").css('white-space', 'normal');
                }

                secondElem.unbind('click').click(function () { Vote(result.Models[1].Id, secondElem); });
                secondElem.find('#id').text(result.Models[1].Id);
                secondElem.find('#title').text(result.Models[1].Title).attr('href', '/FailDetail/Index/' + result.Models[1].Id + "?rand=" + Math.random());
                secondElem.find('#title').attr('title', result.Models[1].Title);
                secondElem.find('#description').text(result.Models[1].Description);
                secondElem.find('#description').attr('title', result.Models[1].Description);
                secondElem.find('.vote').fadeOut(0).text(result.Models[1].Vote);
                if (result.Models[1].ImageId != null) {
                    secondElem.find('#picture').attr('src', '/Common/GetImage/' + result.Models[1].ImageId);
                    secondElem.find('#picture').parent().parent().show();
                    // if there is picture than show truncated description
                    secondElem.find(".compared #description").css('overflow', 'hidden');
                    secondElem.find(".compared #description").css('white-space', 'nowrap');
                } else {
                    secondElem.find('#picture').parent().parent().hide();
                    // if there is no picture than show full description
                    secondElem.find(".compared #description").css('overflow', '');
                    secondElem.find(".compared #description").css('white-space', 'normal');
                }

                firstElem.animate({ left: "+=" + offset + "px" }, "fast");
                secondElem.animate({ left: "-=" + offset + "px" }, "fast");
            }
        }
    });
}

var alreadyClicked = false;
function Vote(id, sender) {
    if (alreadyClicked) return;

    alreadyClicked = true;

    var vote = sender.find(".vote");
    vote.text(vote.text() * 1 + 1);

    $.ajax({
        type: "GET",
        url: "/Common/Vote/" + id,
        dataType: "json",
        contentType: 'application/json',
        success: function (result) {
            if (!result.IsError) {
                var firstVote = firstElem.find('.vote').removeClass('win').removeClass('bloop');
                var secondVote = secondElem.find('.vote').removeClass('win').removeClass('bloop');
                if (firstVote.text() * 1 > secondVote.text() * 1)
                    firstVote.addClass('win').addClass('bloop');
                else if (firstVote.text() * 1 < secondVote.text() * 1)
                    secondVote.addClass('win').addClass('bloop');
                else {
                    firstVote.addClass('win').addClass('bloop');
                    secondVote.addClass('win').addClass('bloop');
                }

                firstVote.fadeIn();
                secondVote.fadeIn();

                setTimeout(function () { alreadyClicked = false; GetFailForCompare(); }, 3000);

                UpdateTopFails();

            }
        }
    });
}

function UpdateTopFails() {

    $.ajax({
        type: "GET",
        url: "/Common/GetTopFailListPartial",
        success: function (result) {

            //$("#topFailsContainer").fadeOut();
            $("#topFailsContainer").html("");
            $("#topFailsContainer").html(result);
            //$("#topFailsContainer").fadeIn();
        }
    });
}