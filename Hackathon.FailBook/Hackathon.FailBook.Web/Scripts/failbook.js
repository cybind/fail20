﻿$(document).ready(function () {
    $('.bokeh-10').everyTime(10, function () {
        $('.bokeh-10').animate({ backgroundPosition: '240px -270px' }, 0);
        $('.bokeh-10').animate({ backgroundPosition: '350px -200px' }, 5100);
        $('.bokeh-10').animate({ backgroundPosition: '180px -150px' }, 5400);
        $('.bokeh-10').animate({ backgroundPosition: '240px -270px' }, 5500);
        $('.bokeh-10').animate({ backgroundPosition: '290px -350px' }, 5500);
        $('.bokeh-10').animate({ backgroundPosition: '240px -270px' }, 6500);
        /* 10500 */
    });
    $('.bokeh-9').everyTime(10, function () {
        $('.bokeh-9').animate({ backgroundPosition: '784px 90px' }, 0);
        $('.bokeh-9').animate({ backgroundPosition: '500px 50px' }, 5100);
        $('.bokeh-9').animate({ backgroundPosition: '400px 100px' }, 5400);
        $('.bokeh-9').animate({ backgroundPosition: '320px 150px' }, 5500);
        $('.bokeh-9').animate({ backgroundPosition: '460px -10px' }, 5500);
        $('.bokeh-9').animate({ backgroundPosition: '784px 90px' }, 6500);
    });
});

function ConsoleOut(message) {
    var debug = true;
    if (debug) console.log(message);
}