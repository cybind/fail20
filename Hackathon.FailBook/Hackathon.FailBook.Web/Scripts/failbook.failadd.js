﻿$(document).ready(function () {

    $("#FailAddTitle").click(function () {
        $(this).parent().removeClass("error");
    });

    $("#BtnFailAdd").click(function () {

        // save fail
        var id = $("#FailAddId").val();
        var title = $("#FailAddTitle").val();
        var description = $("#FailAddDescription").val();
        var imageId = $("#uploadImgId").val();

        if (!title) {
            $("#FailAddTitle").parent().addClass("error");
            return false;
        }

        ConsoleOut("id = " + id);
        ConsoleOut("title = " + title);
        ConsoleOut("description = " + description);
        ConsoleOut("imageId  = " + imageId);

        var newFail = {
            Id: id,
            Title: title,
            Description: description,
            ImageId: imageId
        };

        $.ajax({
            type: "POST",
            timeout: 30000,
            url: "/Common/SaveFail",
            contentType: "application/json",
            data: JSON.stringify(newFail),
            beforeSend: function () {
                //$("#AddToContactsPreloader").show();
            },
            success: function (result) {
                ConsoleOut(result);
                $('#FailAddModal').modal('hide');
            }
        });

    });

    $('#FailAddModal').on('hidden', function () {
        $("#FailAddTitle").val("");
        $("#FailAddDescription").val("");
        $("#uploadImgId").val("");
        $('#dropbox > .preview').remove();
        $('#dropbox > .message').show();
    });

});

